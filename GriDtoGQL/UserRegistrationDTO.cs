﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class UserRegistrationDTO
    {
        public string RegistrationLink { get; set; }
        public string Password { get; set; }
    }
}
