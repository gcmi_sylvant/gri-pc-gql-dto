﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class LoginDTO
    {
        public string LoginEmail { get; set; }
        public string Password { get; set; }
    }
}
