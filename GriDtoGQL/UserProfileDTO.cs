﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class UserProfileDTO
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int? LinkToLegacyId { get; set; }
        public int? RoleTypeId { get; set; }
        public bool AccessToPortal { get; set; }
        public bool Deleted { get; set; }
        public string Token { get; set; }

        public virtual LinkToLegacyDTO LinkToLegacy { get; set; }
        public virtual TypeDTO RoleType { get; set; }
    }
}
