﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class LinkToLegacyDTO
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public virtual ICollection<LinkToLegacyAccountDTO> LinkToLegacyAccounts { get; set; }
    }
}
