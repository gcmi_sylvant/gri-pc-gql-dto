﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class UserActivityDTO
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public DateTime? Connected { get; set; }
        public DateTime Disconnected { get; set; }
        public bool ForcedDisconnection { get; set; }
    }
}
