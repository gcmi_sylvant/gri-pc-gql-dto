﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string LoginEmail { get; set; }
        public bool Deleted { get; set; }
        public int? StatusTypeId { get; set; }

        public virtual TypeDTO StatusType { get; set; }
        public virtual ICollection<UserProfileDTO> UserProfiles { get; set; }
    }
}
