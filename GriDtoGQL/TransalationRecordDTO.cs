﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class TranslationRecordDTO
    {
        public int Id { get; set; }
        public string TableName { get; set; }
        public int TableRowId { get; set; }
        public short LanguageMasterId { get; set; }
        public bool Translated { get; set; }
        public string Value1 { get; set; }
        public string Value2 { get; set; }
        public string Value3 { get; set; }
        public string Value4 { get; set; }
        public string Value5 { get; set; }
        public string Value6 { get; set; }
        public string Value7 { get; set; }
        public string Value8 { get; set; }
        public string Value9 { get; set; }
        public string Value10 { get; set; }
    }
}
