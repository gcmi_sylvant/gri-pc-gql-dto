﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class LinkToLegacyAccountDTO
    {
        public int Id { get; set; }
        public int? LinkToLegacyId { get; set; }
        public int AccountId { get; set; }
        public bool DefaultAccount { get; set; }
    }
}
