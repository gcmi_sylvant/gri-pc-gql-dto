﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class TypeDTO
    {
        public int Id { get; set; }
        public int TypeGroupId { get; set; }
        public string Code { get; set; }
        public bool Deleted { get; set; }

        public virtual IDictionary<string, TranslationRecordDTO> TranslationRecords { get; set; }
    }
}
