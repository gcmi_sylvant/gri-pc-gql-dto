﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GriDtoGQL
{
    public class TypeGroupDTO
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public bool Deleted { get; set; }
        
        public virtual ICollection<TypeDTO> Types { get; set; }

        public virtual IDictionary<string, TranslationRecordDTO> TranslationRecords { get; set; }
    }
}
